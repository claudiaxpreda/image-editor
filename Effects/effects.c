#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BLACKWHITE 15
#define COLOR 16
#define FINISH 11
#define SMOOTH 1
#define BLUR 2
#define SHARPEN 3
#define MEAN 4
#define EMBOSS 5

// Color pixel structure;
typedef struct {
  unsigned char red;
  unsigned char green;
  unsigned char blue;
} rgb;

// Image structure;
typedef struct {
  char type[2];
  int width;
  int height;
  int maxval;
  unsigned char **bw_matrix;
  rgb **color_matrix;
} image;

//Functions for memory allocation;
unsigned char **allocateMatrix(int height, int width) {
  
  unsigned char *data = (unsigned char *)calloc(height * width, sizeof(unsigned char));

  unsigned char **matrix = (unsigned char **)calloc(height, sizeof(unsigned char *));
  int i;

  for (i = 0; i < height; i++) {
    matrix[i] = &(data[width * i]);
  }

  return matrix;
}

rgb **allocateRGBMatrix(int height, int width) {

  rgb *data = (rgb *)calloc(height * width, sizeof(rgb));
  rgb **matrix = (rgb **)calloc(height, sizeof(rgb *));
  int i;

  for (i = 0; i < height; i++) {
    matrix[i] = &(data[width * i]);
  }

  return matrix;
}

// Free the memory;
void freeMatrix(int **matrix, int height) {
  int i;

  for (i = 0; i < height; i++) free(matrix[i]);

  free(matrix);
}

// Reading the image from an input file;
void readInput(const char *fileName, image *img, int *tag) {
  int i;

  FILE *input_file = fopen(fileName, "rb");

  fscanf(input_file, "%s\n", img->type);
  fscanf(input_file, "%d %d\n", &img->width, &img->height);
  fscanf(input_file, "%d\n", &img->maxval);

  if (strcmp(img->type, "P5") == 0) {
    img->bw_matrix = allocateMatrix(img->height, img->width);
    *tag = BLACKWHITE;

    for (i = 0; i < img->height; i++)
      fread(img->bw_matrix[i], sizeof(unsigned char), img->width, input_file);
  }

  if (strcmp(img->type, "P6") == 0) {
    img->color_matrix = allocateRGBMatrix(img->height, img->width);
    *tag = COLOR;

    for (i = 0; i < img->height; i++)
      fread(img->color_matrix[i], sizeof(rgb), img->width, input_file);
  }

  fclose(input_file);
}

// Print the new image in the output file;
void writeData(const char *fileName, image *img) {
  int i;

  FILE *output_file = fopen(fileName, "wb");

  fprintf(output_file, "%s\n", img->type);
  fprintf(output_file, "%d %d\n", img->width, img->height);
  fprintf(output_file, "%d\n", img->maxval);

  if (strcmp(img->type, "P5") == 0) {
    for (i = 0; i < img->height; i++)
      fwrite(img->bw_matrix[i], sizeof(unsigned char), img->width, output_file);
  }

  if (strcmp(img->type, "P6") == 0) {
    for (i = 0; i < img->height; i++) {
      fwrite(img->color_matrix[i], sizeof(rgb), img->width, output_file);
    }
  }

  fclose(output_file);
}

// Applying an effect for a black and white image;
unsigned char **applyEffect(int height, int width, float effect[3][3], unsigned char **matrix) {
  int i, j;

  unsigned char **appliedEffect = allocateMatrix(height, width);

  for (i = 1; i < height - 1; i++) {
    for (j = 1; j < width - 1; j++) {

      float new_pixel =
          matrix[i - 1][j - 1] * effect[0][0] +
          matrix[i - 1][j] * effect[0][1] +
          matrix[i - 1][j + 1] * effect[0][2] +
          matrix[i][j - 1] * effect[1][0] + matrix[i][j] * effect[1][1] +
          matrix[i][j + 1] * effect[1][2] +
          matrix[i + 1][j - 1] * effect[2][0] +
          matrix[i + 1][j] * effect[2][1] + matrix[i + 1][j + 1] * effect[2][2];

      appliedEffect[i][j] = (unsigned char)new_pixel;
    }
  }

  return appliedEffect;
}

// Applying an effect for a color image;
rgb **applyEffectRGB(int height, int width, float effect[3][3], rgb **matrix) {
  int i, j;

  rgb **appliedEffect = allocateRGBMatrix(height, width);

  for (i = 1; i < height - 1; i++) {
    for (j = 1; j < width - 1; j++) {

      float new_red_pixel = matrix[i - 1][j - 1].red * effect[0][0] +
                            matrix[i - 1][j].red * effect[0][1] +
                            matrix[i - 1][j + 1].red * effect[0][2] +
                            matrix[i][j - 1].red * effect[1][0] +
                            matrix[i][j].red * effect[1][1] +
                            matrix[i][j + 1].red * effect[1][2] +
                            matrix[i + 1][j - 1].red * effect[2][0] +
                            matrix[i + 1][j].red * effect[2][1] +
                            matrix[i + 1][j + 1].red * effect[2][2];

      float new_green_pixel = matrix[i - 1][j - 1].green * effect[0][0] +
                              matrix[i - 1][j].green * effect[0][1] +
                              matrix[i - 1][j + 1].green * effect[0][2] +
                              matrix[i][j - 1].green * effect[1][0] +
                              matrix[i][j].green * effect[1][1] +
                              matrix[i][j + 1].green * effect[1][2] +
                              matrix[i + 1][j - 1].green * effect[2][0] +
                              matrix[i + 1][j].green * effect[2][1] +
                              matrix[i + 1][j + 1].green * effect[2][2];

      float new_blue_pixel = matrix[i - 1][j - 1].blue * effect[0][0] +
                             matrix[i - 1][j].blue * effect[0][1] +
                             matrix[i - 1][j + 1].blue * effect[0][2] +
                             matrix[i][j - 1].blue * effect[1][0] +
                             matrix[i][j].blue * effect[1][1] +
                             matrix[i][j + 1].blue * effect[1][2] +
                             matrix[i + 1][j - 1].blue * effect[2][0] +
                             matrix[i + 1][j].blue * effect[2][1] +
                             matrix[i + 1][j + 1].blue * effect[2][2];

      appliedEffect[i][j].red = (unsigned char)new_red_pixel;
      appliedEffect[i][j].green = (unsigned char)new_green_pixel;
      appliedEffect[i][j].blue = (unsigned char)new_blue_pixel;
    }
  }

  return appliedEffect;
}

// Deciding what filter to apply on a black and white image;
unsigned char **getFilter(unsigned char **matrix, int height, int width,
                          int tag) {
  float emboss[3][3] = {
      {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, -1.0f, 0.0f}};

  float mean[3][3] = {
      {-1.0f, -1.0f, -1.0f}, {-1.0f, 9.0f, -1.0f}, {-1.0f, -1.0f, -1.0f}};

  float sharpen[3][3] = {{0.0f, -2.0f / 3.0f, 0.0f},
                         {-2.0f / 3.0f, 11.0f / 3.0f, -2.0f / 3.0f},
                         {0.0f, -2.0f / 3.0f, 0.0f}};

  float blur[3][3] = {{1.0f / 16.0f, 2.0f / 16.0f, 1.0f / 16.0f},
                      {2.0f / 16.0f, 4.0f / 16.0f, 2.0f / 16.0f},
                      {1.0f / 16.0f, 2.0f / 16.0f, 1.0f / 16.0f}};
  float smooth[3][3] = {{1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f},
                        {1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f},
                        {1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f}};

  if (tag == SMOOTH) return applyEffect(height, width, smooth, matrix);

  if (tag == BLUR) return applyEffect(height, width, blur, matrix);

  if (tag == SHARPEN) return applyEffect(height, width, sharpen, matrix);

  if (tag == MEAN) return applyEffect(height, width, mean, matrix);

  return applyEffect(height, width, emboss, matrix);
}

// Deciding what filter to apply on a color image;
rgb **getFilterRGB(rgb **matrix, int height, int width, int tag) {
  float emboss[3][3] = {
      {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, -1.0f, 0.0f}};

  float mean[3][3] = {
      {-1.0f, -1.0f, -1.0f}, {-1.0f, 9.0f, -1.0f}, {-1.0f, -1.0f, -1.0f}};

  float sharpen[3][3] = {{0.0f, -2.0f / 3.0f, 0.0f},
                         {-2.0f / 3.0f, 11.0f / 3.0f, -2.0f / 3.0f},
                         {0.0f, -2.0f / 3.0f, 0.0f}};

  float blur[3][3] = {{1.0f / 16.0f, 2.0f / 16.0f, 1.0f / 16.0f},
                      {2.0f / 16.0f, 4.0f / 16.0f, 2.0f / 16.0f},
                      {1.0f / 16.0f, 2.0f / 16.0f, 1.0f / 16.0f}};
  float smooth[3][3] = {{1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f},
                        {1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f},
                        {1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f}};

  if (tag == SMOOTH) return applyEffectRGB(height, width, smooth, matrix);

  if (tag == BLUR) return applyEffectRGB(height, width, blur, matrix);

  if (tag == SHARPEN) return applyEffectRGB(height, width, sharpen, matrix);

  if (tag == MEAN) return applyEffectRGB(height, width, mean, matrix);

  return applyEffectRGB(height, width, emboss, matrix);
}

// Create a new data type for MPI communication;
void createDataType(MPI_Datatype *mpi_rgb) {

  int no_elements = 3;
  int blocklengths[3] = {1, 1, 1};

  MPI_Aint displacements[3] = {offsetof(rgb, red), offsetof(rgb, green),
                               offsetof(rgb, blue)};
  MPI_Datatype datatypes[3] = {MPI_UNSIGNED_CHAR, MPI_UNSIGNED_CHAR,
                               MPI_UNSIGNED_CHAR};
  MPI_Type_create_struct(no_elements, blocklengths, displacements, datatypes,
                         mpi_rgb);
  MPI_Type_commit(mpi_rgb);
}

int getFilterTag(char *filter) {

  if (strcmp(filter, "smooth") == 0) return SMOOTH;

  if (strcmp(filter, "blur") == 0) return BLUR;

  if (strcmp(filter, "sharpen") == 0) return SHARPEN;

  if (strcmp(filter, "mean") == 0) return MEAN;

  return EMBOSS;
}

int main(int argc, char *argv[]) {

  int rank;
  int nProcesses;

  int indexes[2];
  unsigned char **bytes;
  rgb **colorbytes;

  //Init the connection;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nProcesses);
  MPI_Status status_1;
  MPI_Status status_2;
  MPI_Datatype MPI_RGB;
  createDataType(&MPI_RGB);
  int i, j;

  if (rank == 0) {
    image input;
    image output;
    int imgTag;

    //Reading the image;
    readInput(argv[1], &input, &imgTag);

    strcpy(output.type, input.type);
    output.height = input.height;
    output.width = input.width;
    output.maxval = input.maxval;

    // Divide the matrix line between processes;
    int part = (input.height - 2) / nProcesses;
    indexes[0] = part;
    indexes[1] = input.width;

    //Init the parameters based on the image type;
    if (imgTag == BLACKWHITE) {

      output.bw_matrix = allocateMatrix(output.height, output.width);
      bytes = allocateMatrix(indexes[0] + 2, indexes[1]);
      memcpy(&output.bw_matrix[0][0], &input.bw_matrix[0][0],
             input.width * input.height);
    } else {

      output.color_matrix = allocateRGBMatrix(output.height, output.width);
      colorbytes = allocateRGBMatrix(indexes[0] + 2, indexes[1]);
      memcpy(&output.color_matrix[0][0], &input.color_matrix[0][0],
             input.width * input.height * sizeof(rgb));
    }

    int filterTag;

    // Apply filters;
    for (i = 3; i < argc; i++) {

      indexes[0] = part;
      filterTag = getFilterTag(argv[i]);

      //Process 0 applies the given effect;
      if (imgTag == BLACKWHITE) {

        memcpy(&bytes[0][0], &output.bw_matrix[0][0],
               (indexes[0] + 2) * indexes[1]);
        bytes = getFilter(bytes, indexes[0] + 2, indexes[1], filterTag);
      } else {

        memcpy(&colorbytes[0][0], &output.color_matrix[0][0],
               (indexes[0] + 2) * indexes[1] * sizeof(rgb));
        colorbytes = getFilterRGB(colorbytes, indexes[0] + 2, indexes[1], filterTag);
      }

      // Sending the data to the other processes;
      for (j = 1; j < nProcesses; j++) {

        if (j == nProcesses - 1) {
          indexes[0] += (input.height - 2) % nProcesses;
        }

        MPI_Send(&indexes, 2, MPI_INT, j, imgTag, MPI_COMM_WORLD);

        if (imgTag == BLACKWHITE)
          MPI_Send(&output.bw_matrix[j * part][0],
                   (indexes[0] + 2) * indexes[1], MPI_UNSIGNED_CHAR, j,
                   filterTag, MPI_COMM_WORLD);
        else
          MPI_Send(&output.color_matrix[j * part][0],
                   (indexes[0] + 2) * indexes[1], MPI_RGB, j, filterTag,
                   MPI_COMM_WORLD);
      }

      // Copies the result in the output matrix;
      indexes[0] = (input.height - 2) / nProcesses;
      if (imgTag == 15) {

        for (j = 1; j <= indexes[0]; j++) {
          memcpy(&output.bw_matrix[j][1], &bytes[j][1], indexes[1] - 2);
        }
      } else {

        for (j = 1; j <= indexes[0]; j++) {
          memcpy(&output.color_matrix[j][1], &colorbytes[j][1],
                 (indexes[1] - 2) * sizeof(rgb));
        }
      }

      // Waiting for the data from the other processes and 
      //completing the output matrix;
      for (j = 1; j < nProcesses; j++) {

        if (j == nProcesses - 1) {
          indexes[0] += (input.height - 2) % nProcesses;
        }

        if (imgTag == BLACKWHITE) {

          unsigned char **recv_bytes = allocateMatrix(indexes[0] + 2, indexes[1]);
          MPI_Recv(&recv_bytes[0][0], (indexes[0] + 2) * indexes[1],
                   MPI_UNSIGNED_CHAR, j, 0, MPI_COMM_WORLD, &status_1);
          int k;

          for (k = 1; k <= indexes[0]; k++) {
            memcpy(&output.bw_matrix[j * part + k][1], &recv_bytes[k][1],
                   indexes[1] - 2);
          }
        } else {

          rgb **recv_bytes = allocateRGBMatrix(indexes[0] + 2, indexes[1]);
          MPI_Recv(&recv_bytes[0][0], (indexes[0] + 2) * indexes[1], MPI_RGB, j,
                   0, MPI_COMM_WORLD, &status_1);
          int k;

          for (k = 1; k <= indexes[0]; k++) {
            memcpy(&output.color_matrix[j * part + k][1], &recv_bytes[k][1],
                   (indexes[1] - 2) * sizeof(rgb));
          }
        }
      }
    }

    // Sending the stop tag;
    for (j = 1; j < nProcesses; j++)
      MPI_Send(&indexes, 2, MPI_INT, j, FINISH, MPI_COMM_WORLD);

    writeData(argv[2], &output);

  } else {
    // Waiting to receive the data in order tu apply the given effect;
    while (1) {

      MPI_Recv(&indexes, 2, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status_1);

      if (status_1.MPI_TAG != 11) {

        if (status_1.MPI_TAG == BLACKWHITE) {

          bytes = allocateMatrix(indexes[0] + 2, indexes[1]);
          MPI_Recv(&bytes[0][0], (indexes[0] + 2) * indexes[1],
                   MPI_UNSIGNED_CHAR, 0, MPI_ANY_TAG, MPI_COMM_WORLD,
                   &status_2);
          
          bytes = getFilter(bytes, indexes[0] + 2, indexes[1], status_2.MPI_TAG);
          MPI_Send(&bytes[0][0], (indexes[0] + 2) * indexes[1],
                   MPI_UNSIGNED_CHAR, 0, 0, MPI_COMM_WORLD);
        } else {

          colorbytes = allocateRGBMatrix(indexes[0] + 2, indexes[1]);
          MPI_Recv(&colorbytes[0][0], (indexes[0] + 2) * indexes[1], MPI_RGB, 0,
                   MPI_ANY_TAG, MPI_COMM_WORLD, &status_2);

          colorbytes = getFilterRGB(colorbytes, indexes[0] + 2, indexes[1],
                                    status_2.MPI_TAG);
          MPI_Send(&colorbytes[0][0], (indexes[0] + 2) * indexes[1], MPI_RGB, 0,
                   0, MPI_COMM_WORLD);
        }

      } else {
        // Stopping the process;
        break;
      }
    }
  }

  MPI_Finalize();
  return 0;
}
