### Claudia Preda

# **Image Editor - Readme**

*	This project is an application for my Parallel and Distributed Algorithms class 
	and its main scope is to familiarize myself with this two concepts(Parallel and Distributed).
* 	This application is just published on bitbucket, I did not work with a repository when
	writing the code;
	
### *Resize:*
*	The first part of this project consists in creating a parallel program which can resize a
	pnm/png files based on resize_factor given as an input.
*	The given argumets of this program are:
	** **argv[1]:** the input file (pnm/pgm);
	** **argv[2]:** the output file(pnm/pgm);
	** **argv[3]:** resize_factor;
	** **argv[4]:** number of threads**;
* It is developed using C library "pthreads.h";

### *Effects:*
*	The second part of this projects is applying a number of effects given as inpur by the user
	to a pnm/pgm file; 
* It is developed using MPI, and the work is equally distributed between the processes;
* Written in C;
* Only process with the id equal to 0 cand read or write the files;
* The available effects are: EMBOSS, SHARPEN, BLUR, SMOOTH, MEAN REMOVAL;
* The given arguments are the following:
	** **argv[1]:** the input file (pnm/pgm);
	** **argv[2]:** the output file(pnm/pgm);
	** **argv[3-n]:** effects, as many as the user wants;
	
