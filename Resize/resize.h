//Preda Claudia 

#ifndef HOMEWORK_H
#define HOMEWORK_H

//Black-white image, resize_factor even;
#define BW_EVEN  1
//Color image, resize_factor even;
#define COLOR_EVEN 2
//Black-white image, resize_factor odd;
#define BW_ODD  5
//Color image, resize_factor odd;
#define COLOR_ODD 4
//resize_factor even;
#define EVEN 6
////resize_factor odd;
#define ODD 7
//Gaussian Kernel sum;
#define GAUSSIAN_SUM 16
//Gaussian Kernel dimension;
#define DIM 3

//Color pixel representation containing a pixel 
//from each rgb color;
typedef struct {
  unsigned char rd; //red pixel;
  unsigned char gr; //green pixel;
  unsigned char bl; //blue pixel;
} rgb;

//Image structure based on pnm/pgm format;
typedef struct {
  char type[2];
  int width;
  int height;
  int maxval;
  unsigned char **bw_matrix;
  rgb **color_matrix;
} image;

//Parameters of a thread structure;
typedef struct {
  int thread_id;
  image *in;
  image *out;
  int lines_start;
  int lines_end;
} threadParam;

unsigned char **allocate_matrix(int width, int height);

rgb **allocate_rgb_matrix(int width, int height);

void free_matrix(void **matrix, int height);

void* threadFunction(void *var);

void getOptions(image *img, int *option);

int getPixelBlack(unsigned char **matrix, int start_row, int start_col);

rgb getPixelColor(rgb **matrix, int start_row, int start_col);

void* threadFunction(void *var);

void readInput(const char * fileName, image *img);

void writeData(const char * fileName, image *img);

void resize(image *in, image * out);



#endif /* HOMEWORK_H */
