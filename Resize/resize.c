//Preda Claudia 

#include "resize.h"
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>

int num_threads;
int resize_factor;
int gaussianKernel[3][3];
int option;
int minimize;

//Memory allocation functions
unsigned char **allocate_matrix(int width, int height) {
  unsigned char **matrix;
  int i;

  matrix = (unsigned char**)calloc(height, sizeof(unsigned char*));

  for (i = 0; i < height; i++) {
    matrix[i] = (unsigned char*)calloc(width,sizeof (unsigned char));
  }

  return matrix;
}

rgb **allocate_rgb_matrix(int width, int height) {
  rgb **matrix;
  int i;

  matrix = (rgb**)calloc(height, sizeof(rgb*));

  for (i = 0; i < height; i++) {
    matrix[i] = (rgb*)calloc(width,sizeof (rgb));
  }

  return matrix;
}


//Free memory;
void free_matrix(void **matrix, int height) {
  int i ;

  for (i = 0 ; i < height; i++) {
    free(matrix[i]);
  }

  free(matrix);
}

//Initialize Gaussian kernel based on resize_factor value;
void initKernel(int parity) {
  int i, j;

  if (parity == ODD) {
    gaussianKernel[0][0] = 1;
    gaussianKernel[0][1] = 2;
    gaussianKernel[0][2] = 1;
    gaussianKernel[1][0] = 2;
    gaussianKernel[1][1] = 4;
    gaussianKernel[1][2] = 2;
    gaussianKernel[2][0] = 1;
    gaussianKernel[2][1] = 2;
    gaussianKernel[2][2] = 1;
  }

  if (parity == EVEN) {
    for(i = 0; i <3; i++)
      for(j = 0; j <3; j++)
      gaussianKernel[i][j] = 1;
  }
}


//Deciding the type of image based on resize_factor value and its format;
void getOptions(image *img, int *option) {
  if (strcmp(img->type, "P5") == 0 && resize_factor % 2 == 0 )
    *option = BW_EVEN;

  if (strcmp(img->type, "P5") == 0 && resize_factor % 2 == 1 )
    *option = BW_ODD;

  if (strcmp(img->type, "P6") == 0 && resize_factor % 2 == 0 )
      *option = COLOR_EVEN;

  if (strcmp(img->type, "P6") == 0 && resize_factor % 2 == 1 )
      *option = COLOR_ODD;
}

//Compress resize_factor*resize_factor pixels into one for black and white images;
int getPixelBlack(unsigned char **matrix, int start_row, int start_col) {
  int i,j;
  int elem_sum = 0;

  for (i = start_row * resize_factor; i < (start_row + 1) * resize_factor; i++)
    for (j = start_col * resize_factor; j < (start_col + 1) * resize_factor; j++)
      elem_sum += matrix[i][j] * gaussianKernel[i % DIM][j % DIM];

  //Calculating the pixel;
  return elem_sum / minimize;
}


//Compress resize_factor*resize_factor pixels into one for color images;
rgb getPixelColor(rgb **matrix, int start_row, int start_col) {
  int i,j;
  rgb aux;

  int elem_sum_rd = 0;
  int elem_sum_gr = 0;
  int elem_sum_bl = 0;

  for (i = start_row * resize_factor; i < (start_row + 1) * resize_factor; i++)
    for (j = start_col * resize_factor; j < (start_col + 1) * resize_factor; j++) {
      elem_sum_rd += matrix[i][j].rd * gaussianKernel[i % DIM][j % DIM];
      elem_sum_gr += matrix[i][j].gr * gaussianKernel[i % DIM][j % DIM];
      elem_sum_bl += matrix[i][j].bl * gaussianKernel[i % DIM][j % DIM];
    }

    //Calculating the value for each rgb pixel;

    aux.rd = (unsigned char) (elem_sum_rd / minimize);
    aux.gr = (unsigned char) (elem_sum_gr / minimize);
    aux.bl = (unsigned char) (elem_sum_bl / minimize);
    return aux;
}

//Reading the image given in the pnm/pgm format;
void readInput(const char * fileName, image *img) {
  int i;
  FILE *input_file = fopen(fileName, "rb");

  //Reading the type, dimensions and max_gray value;
  fscanf(input_file, "%s\n", img->type);
  fscanf(input_file, "%d %d\n", &img->width, &img->height);
  fscanf(input_file, "%d\n", &img->maxval);

  //Reading the pixels values according to the image type;
  if (strcmp(img->type,"P5") == 0) {
    img->bw_matrix = allocate_matrix(img->width, img->height);
    for (i = 0; i < img->height; i++)
      fread(img->bw_matrix[i], sizeof(unsigned char), img->width, input_file);
  }

  if (strcmp(img->type,"P6") == 0) {
    img->color_matrix = allocate_rgb_matrix(img->width, img->height);
    for(i = 0; i < img->height; i++)
      fread(img->color_matrix[i], sizeof(rgb), img->width, input_file);
  }

  fclose(input_file);
}

//Writing in the output file
void writeData(const char * fileName, image *img) {
  int i;
  FILE *output_file = fopen(fileName, "wb");

  //Writing the type, dimensions and max_gray value;
  fprintf(output_file, "%s\n",img->type );
  fprintf(output_file, "%d %d\n",img->width, img->height);
  fprintf(output_file, "%d\n", img->maxval);

  //Writing the matrix based on type;
  if (strcmp(img->type,"P5") == 0) {
   for (i = 0; i < img->height; i++)
      fwrite(img->bw_matrix[i], sizeof(unsigned char), img->width, output_file);

   free_matrix((void **)img->bw_matrix, img->height);
  }

  if (strcmp(img->type,"P6") == 0) {
    for (i = 0; i < img->height; i++)
      fwrite(img->color_matrix[i], sizeof(rgb), img->width, output_file);

    free_matrix((void **)img->color_matrix, img->height);
  }

  fclose(output_file);
}

//The function executed by a thread;
void* threadFunction(void *var) {
	threadParam param = *(threadParam*)var;
  int i, j;

  //Compressing resize_factor*resize_factor pixels into one
  //based on the image type;
  if (option == BW_EVEN || option == BW_ODD) {
    for ( i = param.lines_start ; i < param.lines_end; i++) {
      for ( j = 0; j < param.out->width; j++) {
            param.out->bw_matrix[i][j] = (unsigned char) getPixelBlack(param.in->bw_matrix, i, j);
          }
      }
  }

  if (option == COLOR_EVEN || option == COLOR_ODD) {
    for (i = param.lines_start ; i < param.lines_end; i++)
      for ( j = 0; j < param.out->width; j++) {
        param.out->color_matrix[i][j] = getPixelColor(param.in->color_matrix, i, j);
      }
  }

  return NULL;
}

//Resize an image;
void resize(image *in, image * out) {
  int i;
  int interval;

  pthread_t tid[num_threads];
  int thread_id[num_threads];

  getOptions(in, &option);

  //Init parameters based on the image type given as inoput;
  if (option == BW_EVEN || option == COLOR_EVEN ) {
        minimize= resize_factor * resize_factor;
        initKernel(EVEN);
  }

  if (option == BW_ODD || option == COLOR_ODD ) {
        minimize =  GAUSSIAN_SUM;
        initKernel(ODD);
  }

  //Init output image and calculating its parametres;
  strcpy(out->type, in->type);
  out->width = in->width / resize_factor;
  out->height = in->height / resize_factor;
  out->maxval = in->maxval;

  if (option == BW_EVEN || option == BW_ODD)
    out->bw_matrix = allocate_matrix(out->width, out->height);

  if(option == COLOR_EVEN || option == COLOR_ODD){
    out->color_matrix = allocate_rgb_matrix(out->width, out->height);
  }

  //Number of lines executed by a thread
  interval = out->height / num_threads;

  for(i = 0;i < num_threads; i++)
		thread_id[i] = i;

  threadParam *param = (threadParam*)malloc(num_threads*sizeof(threadParam));

  for (i = 0; i < num_threads; i++) {
      //Calculating the parameters for every thread;
      param[i].thread_id = thread_id[i];
      param[i].in = in;
      param[i].out = out;

      //First line to be executed by a thread;
      param[i].lines_start = i * interval;

      //Last line to be executed by a thread;
      if ( i != num_threads - 1) {
        param[i].lines_end = (i+1)*interval;
      }
      else {
        //The last thread executes the remaining lines;
        param[i].lines_end = out->height;
      }

  		pthread_create(&(tid[i]), NULL, threadFunction, &(param[i]));
  }

  for(i = 0; i < num_threads; i++) {
    pthread_join(tid[i], NULL);
  }
}
